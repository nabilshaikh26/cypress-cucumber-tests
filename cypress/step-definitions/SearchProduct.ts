/* eslint-disable import/no-extraneous-dependencies */
/// <reference types="cypress" />

import {
  Given, When, Then, But,
} from 'cypress-cucumber-preprocessor/steps';

Given('Visitor visits {string} page', (pageName) => {
  cy.visit(pageName);
});

When('Visitor search for {string}', (inputString) => {
  cy.get('[id=search_query_top]').type(inputString);
  cy.get('.button-search').click();
});

Then('Visitor sees all the search results listed', () => {
  cy.get('.product-container').should('be.visible');
});

Then('Visitor sees a message as {string}', (message) => {
  cy.contains(message);
});

But('without any search results', () => {
  cy.get('.product-container').should('not.exist');
});

When('Visitor search for a product without entering its name', () => {
  cy.get('.button-search').click();
});
