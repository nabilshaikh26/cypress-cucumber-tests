Feature: Search for a product

As a visitor of the ecommerce website
I need a product search functionality
So that I can search for a product of my choice of what I want to purchase.
  
  Scenario: Search for a valid product
    Given Visitor visits '/' page
    When Visitor search for 'shirt'
    Then Visitor sees all the search results listed
  
  Scenario: Search for an invalid product
    Given Visitor visits '/' page
    When Visitor search for 'shrt'
    Then Visitor sees a message as 'No results were found for your search "shrt"'
    But without any search results

  Scenario: Search for a product but without entering any keyword
    Given Visitor visits '/' page
    When Visitor search for a product without entering its name
    Then Visitor sees a message as 'Please enter a search keyword'